<?php
    require_once('include/session.php');

    if ($_SESSION["logged"]) {
        header('Location: index.php');
    } else {
        if ((isset($_POST['login']) || isset($_POST['password'])) &&
           ((!isset($_POST['login']) || !isset($_POST['password'])) ||
           (empty($_POST['login']) || empty($_POST['password'])))) {
            $error_message = "All fields are mandatory!";
        } else if (isset($_POST['login']) && isset($_POST['password'])) {
            // SQL request to check user login
        }

        require_once("include/header.php");
?>
        <div id="main">
            <div class="wrapper">
                <div class="row">
                    <div class="col-l-12 col-m-12 col-s-12">
                        <form action="login.php" method="POST">
                            <fieldset>
                                <input type="text" name="login" placeholder="Your login" value="" />
                                <input type="password" name="password" placeholder="Your password" value="" />
                            </fieldset>
                            <fieldset>
                                <input type="submit" value="Login" />
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
<?php
        require_once("include/footer.php");
    }
?>
