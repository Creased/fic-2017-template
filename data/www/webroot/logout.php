<?php
    ob_start();

    if (isset($_COOKIE["session"])) {
        setcookie("session", null, 1, '/');
    }

    if (isset($_SESSION)) {
        unset($_SESSION);
    }

    session_destroy();

    header("Location: index.php");

    ob_get_clean();
