<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>

        <title>Icarus &#8211; URL Inspector</title>

        <link rel="icon" href="images/favicon.ico" type="image/x-icon">

        <link rel="stylesheet" media="all" href="css/main.css">
        <link rel="stylesheet" media="all" href="css/fontello.css">
    </head>
    <body>
        <div id="header">
            <div class="wrapper">
                <div class="row">
                    <div class="col-l-7 col-m-7 col-s-12">
                        <h1 class="page-header">{ Icarus } <span class="small">by Creased and DrStache</span></h1>
                    </div><!--
                    --><div id="nav" class="col-l-5 col-m-5 col-s-12">
                        <ul>
                            <li><a href="index.php">Home</a></li><!--
<?php if ($_SESSION["logged"] == 0) { ?>
                            --><li><a href="register.php">Register</a></li><!--
                            --><li><a href="login.php">Login</a></li>
<?php } else { ?>
                            --><li><a href="scan.php">Scan</a></li><!--
                            --><li><a href="status.php">Status</a></li><!--
                            --><li><a href="logout.php">Logout</a></li>
<?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
