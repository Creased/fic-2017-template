<?php
    $db_host = "localhost";
    $db_name = "db";
    $db_login = "user";
    $db_pass = "ch4113ng3_p455w0rd_my5q1";
  
    try {
        $bdd = new PDO("mysql:host=" . $db_host . ";dbname=" . $db_name . ";charset=utf8", $db_login, $db_pass);
    } catch (Exception $e) {
        die("An error occured while trying to connect to backend database.");
    }
  
    function generateHash($pass) {
         $salt = "faNP3pD_8#";
         return hash("sha256", $salt . hash("sha256", $pass));
    }
