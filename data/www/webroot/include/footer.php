        <div id="footer">
            <div class="wrapper">
                <div class="row">
                    <div class="col-s-12 col-m-12 col-l-4"></div><!--
                    --><div class="col-s-12 col-m-12 col-l-4">
                        <p class="legal">
                            <a class="icons" rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/" title="Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International">
                                <i class="icon-cc"></i><!--
                                --><i class="icon-cc-by"></i><!--
                                --><i class="icon-cc-nc"></i><!--
                                --><i class="icon-cc-sa"></i>
                            </a>
                            <span>Except where otherwise noted, content on this site is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International license.</span>
                        </p>
                    </div><!--
                    --><div class="col-s-12 col-m-12 col-l-4"></div>
                </div>
            </div>
        </div>
        <script src="js/jquery.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
