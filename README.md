FIC Challenge 2017 template
===========================

## Informations ##

### File Structure ###

	data        # Directory linked to VirtualBox API (if applicable)
	├─── build  # Data used to build containers (i.e., DockerFile)
	├─── conf   # Configuration data used by services
	├─── data   # Raw data used by services
	└─── log    # Logs issued by services

## Setup ##

### Requirements ###

- [Docker](https://docs.docker.com/engine/installation/);
- [Docker Compose](https://docs.docker.com/compose/install/);
- Less time to prepare environment, more time to develop.

### Download ###

```bash
git clone https://git.bmoine.fr/fic-2017-template challenge
pushd challenge/
```

### Build ###

Build of containers based on docker-compose.yml:

```bash
docker-compose pull
docker-compose build
```

## Start ##

To get it up, please consider using:

```bash
docker-compose up -d
```

### Browse your applications ###

When done, turn on your web browser and crawl your Docker container (e.g., [http://127.0.0.1/](http://127.0.0.1/)).

## Live display of logs ##

```bash
docker-compose logs --follow
```

## Run bash on container ##

Template:

```bash
docker-compose exec SERVICE COMMAND
```

Example:

```bash
docker-compose exec db bash
```

Then you will be able to manage your configuration files, debug daemons and much more...
